# Laravel-vue-color-picker-service

Simple service for select a color from an image and take associated data

- HEX
- RGB
- WebSafe HEX
- Color popular name
- HSL
- CMYK
- Zooming image near cursor =)


## Require

- [Docker](https://www.docker.com/)
- [Docker-compose](https://docs.docker.com/compose/install/)

## Tech
- [Laravel](https://laravel.com/)
- [Vue](https://vuejs.org/)
- [Js Canvas](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial)
- [Ant design](https://www.antdv.com/)


## Installation

Setup .env
- app-root/.env
- app-root/src/backend/.env

Setup server name 
- app-root/.docker/nginx/conf.d/backend.conf

Setup app environments
```sh
docker-compose up -d --build
docker exec -it laravel-color-picker-service_backend_1 sh
composer install
npm install
npm run mix
```

## License

MIT

**Free Software, Hell Yeah!**
