<?php

namespace App\Providers;

use App\Repositories\ColorNamesRepository\ColorNamesRepository;
use App\Repositories\ColorNamesRepository\IColorNamesRepository;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IColorNamesRepository::class, ColorNamesRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
