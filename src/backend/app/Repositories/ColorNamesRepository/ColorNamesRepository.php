<?php


namespace App\Repositories\ColorNamesRepository;


use Illuminate\Support\Facades\Storage;

class ColorNamesRepository implements IColorNamesRepository
{

    public function getAllAsJson(): string
    {
        $result = file_get_contents(storage_path('/static-data/color-picker/colornames.json'));
        return json_encode($result);
    }
}
