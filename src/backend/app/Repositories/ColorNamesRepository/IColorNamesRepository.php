<?php

namespace App\Repositories\ColorNamesRepository;


interface IColorNamesRepository
{
    public function getAllAsJson(): string;
}
