<?php


namespace App\Services\ColorNamesService;


use App\Repositories\ColorNamesRepository\IColorNamesRepository;

class ColorNamesService
{
    public function getColorNamesAsJson(): string
    {
        /** @var IColorNamesRepository $repository */
        $repository = app(IColorNamesRepository::class);
        $result = $repository->getAllAsJson();
        return $result;
    }
}
