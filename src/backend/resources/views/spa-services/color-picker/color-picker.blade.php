@extends('layouts.app')

@section('content')
    <div class="ant-layout">

        <div class="ant-layout-content">
            <color-picker :color-names="{{json_decode($colorNamesJson)}}"></color-picker>
        </div>
    </div>

@endsection

