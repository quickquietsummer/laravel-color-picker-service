function fallbackCopyTextToClipboard(text) {
    var textArea = document.createElement("textarea");
    textArea.value = text;
    // textArea.style.display='none';
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        var successful = document.execCommand("copy");
        var msg = successful ? "successful" : "unsuccessful";
        console.log("Fallback: Copying text command was " + msg);
    } catch (err) {
        console.error("Fallback: Oops, unable to copy", err);
    }

    document.body.removeChild(textArea);
}

export async function copyTextToClipboard(text) {
    return new Promise((resolve, reject) => {
        if (!navigator.clipboard) {
            fallbackCopyTextToClipboard(text);
            resolve();
            // return;
        }
        navigator.clipboard.writeText(text).then(
            function() {
                console.log("Async: Copying to clipboard was successful!");
                resolve();
            },
            function(err) {
                console.error("Async: Could not copy text: ", err);
                reject();
            }
        );
    });
}
