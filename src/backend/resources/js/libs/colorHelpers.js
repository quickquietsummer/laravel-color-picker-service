/**
 * @typedef {Object} IRgb
 * @property {number} r
 * @property {number} g
 * @property {number} b
 */

/**
 *
 * @param {*} hex
 * @returns {IRgb | null} rgb object
 */

export function hexToRgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    // noinspection ConditionalExpressionJS
    return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        }
        : null;
}

/**
 *
 * @param {IRgb} backgroundColorRgb
 * @returns {'black'|'white'}
 */
export function getFontColorContrasted(backgroundColorRgb) {
    // http://www.w3.org/TR/AERT#color-contrast
    // noinspection OverlyComplexArithmeticExpressionJS,MagicNumberJS
    const brightness = Math.round(
        (backgroundColorRgb.r * 299 +
            backgroundColorRgb.g * 587 +
            backgroundColorRgb.b * 114) /
        1000
    );
    // noinspection ConditionalExpressionJS,MagicNumberJS
    return 125 < brightness ? "black" : "white";
}

// noinspection JSUnusedGlobalSymbols
/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   {number}  h       The hue
 * @param   {number}  s       The saturation
 * @param   {number}  l       The lightness
 * @return  {IRgb}           The RGB representation
 */
export function hslToRgb(h, s, l) {
    let r, g, b;

    // noinspection EqualityComparisonWithCoercionJS,NestedAssignmentJS
    if (0 == s) { // jshint ignore:line
        // noinspection NestedAssignmentJS,AssignmentResultUsedJS
        r = g = b = l; // achromatic
    } else {
        const hueToRgb = function hueToRgb(p, q, t) {
            if (0 > t) {
                t += 1;
            }
            if (1 < t) {
                t -= 1;
            }
            if (t < 1 / 6) {
                return p + (q - p) * 6 * t;
            }
            if (t < 1 / 2) {
                return q;
            }
            if (t < 2 / 3) {
                // noinspection OverlyComplexArithmeticExpressionJS
                return p + (q - p) * (2 / 3 - t) * 6;
            }
            return p;
        };

        let q;
        // noinspection MagicNumberJS
        if (0.5 > l) {
            q = l * (1 + s);
        } else {
            q = l + s - l * s;
        }
        const p = 2 * l - q;
        r = hueToRgb(p, q, h + 1 / 3);
        g = hueToRgb(p, q, h);
        b = hueToRgb(p, q, h - 1 / 3);
    }

    const upBound = 255;
    r = Math.round(r * upBound);
    g = Math.round(g * upBound);
    b = Math.round(b * upBound);

    return {r, g, b};
}

/**
 * @typedef {Object} IHsl
 * @property {number} h
 * @property {number} s
 * @property {number} l
 */

/**
 *
 * @param {number} r
 * @param {number} g
 * @param {number} b
 * @returns {IHsl} hsl object
 */
export function rgbToHsl(r, g, b) {
    const upBound = 255;
    (r /= upBound);
    (g /= upBound);
    (b /= upBound);
    const max = Math.max(r, g, b),
        min = Math.min(r, g, b);
    let h,
        s,
        l = (max + min) / 2;

    // noinspection EqualityComparisonWithCoercionJS,AssignmentResultUsedJS
    if (max == min) { // jshint ignore:line
        // noinspection NestedAssignmentJS,AssignmentResultUsedJS
        h = s = 0; // achromatic
    } else {
        const d = max - min;
        // noinspection ConditionalExpressionJS,MagicNumberJS
        s = 0.5 <= l ? d / (2 - (max + min)) : d / (max + min);
        switch (max) {
            case r:
                // noinspection MagicNumberJS,PointlessArithmeticExpressionJS
                h = ((g - b) / d + 0) * 60;
                break;
            case g:
                // noinspection MagicNumberJS
                h = ((b - r) / d + 2) * 60;
                break;
            case b:
                // noinspection MagicNumberJS
                h = ((r - g) / d + 4) * 60;
                break;
        }
    }
    // if (!h) {
    //     throw new Error("No h parameter");
    // }
    h = +h.toFixed(2);
    // noinspection PointlessArithmeticExpressionJS
    s = +((s / 1) * 100).toFixed(2);
    // noinspection PointlessArithmeticExpressionJS
    l = +((l / 1) * 100).toFixed(2);
    return {h, s, l};
}

/**
 * @typedef {Object} ICmyk
 * @property {number} c
 * @property {number} m
 * @property {number} y
 * @property {number} k
 */

/**
 *
 * @param {*} r
 * @param {*} g
 * @param {*} b
 * @param {boolean} normalized
 * @returns {ICmyk} cmyk object
 */
export function rgbToCmyk(r, g, b, normalized) {
    const upBound = 255;
    let c = 1 - r / upBound;
    let m = 1 - g / upBound;
    let y = 1 - b / upBound;


    let k = Math.min(c, Math.min(m, y));

    c = (c - k) / (1 - k);
    m = (m - k) / (1 - k);
    y = (y - k) / (1 - k);

    if (!normalized) {
        const number = 10000;
        c = Math.round(c * number) / 100;
        m = Math.round(m * number) / 100;
        y = Math.round(y * number) / 100;
        k = Math.round(k * number) / 100;
    }

    if (isNaN(c)) {
        c = 0;
    }
    if (isNaN(m)) {
        m = 0;
    }
    if (isNaN(y)) {
        y = 0;
    }
    if (isNaN(k)) {
        k = 0;
    }


    return {
        c: c,
        m: m,
        y: y,
        k: k
    };
}

/**
 *
 * @param r
 * @param g
 * @param b
 * @returns {string} HEX without #
 */
export function rgbToHex(r, g, b) {
    if (255 < r || 255 < g || 255 < b) {
        throw "Invalid color component";
    }
    return ((r << 16) | (g << 8) | b).toString(16);
}

