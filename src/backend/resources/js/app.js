import "./bootstrap";
import Vue from "vue";
import antDesign from "ant-design-vue";
import Vue2TouchEvents from "vue2-touch-events";
import ColorPicker from "./components/ColorPicker/ColorPicker.vue";

Vue.component("color-picker", ColorPicker);
Vue.use(antDesign);
Vue.use(Vue2TouchEvents);


// eslint-disable-next-line no-unused-vars
const app = new Vue({
    el: "#app"
});
