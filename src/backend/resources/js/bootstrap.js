import axios from "axios";
import * as _ from "lodash";
import * as jQuery from "jquery";
import * as Popper from "popper.js";
import "bootstrap";

axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

/**
 * @type  {HTMLMetaElement | null}
 */
let token = document.head?.querySelector('meta[name="csrf-token"]');

if (token) {
    axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
} else {
    console.error(
        "CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token"
    );
}
